package br.com.infnet.davirocha.assessmentjavaandroid;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import android.util.Log;
import android.view.View;
import android.widget.Button;

import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.FirebaseApp;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

import br.com.infnet.davirocha.assessmentjavaandroid.Model.User;

public class MainActivity extends AppCompatActivity{

    private TextView cpf, nome;
    private Button sair;

    final private DatabaseReference databaseReference = FirebaseDatabase.getInstance().getReference("user");
    final private FirebaseAuth mAuth = FirebaseAuth.getInstance();
    final private String id = mAuth.getUid();
    private String uidUser, userName;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        cpf = (TextView) findViewById(R.id.cpf);
        nome = (TextView) findViewById(R.id.nome);
        sair = (Button) findViewById(R.id.btn_sair);

        Intent i = getIntent();
        uidUser = i.getStringExtra("email");
        userName = i.getStringExtra("userName");

        databaseReference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if (dataSnapshot.exists()){
                    User user;
                    for (DataSnapshot ds : dataSnapshot.getChildren()){
                        user = ds.getValue(User.class);
                        //Toast.makeText(MainActivity.this, user.getNome() + "Logado com sucesso!!!", Toast.LENGTH_SHORT).show();
                        if (user.getId().equals(id)){
                            cpf.setText(user.getCpf());
                            nome.setText(user.getNome());
                        }
                    }
                    if (!dataSnapshot.exists()){
                        Intent i = new Intent(getBaseContext(), RegisterActivity.class);
                        i.putExtra("userEmail", uidUser);
                        i.putExtra("userName", userName);
                        startActivity(i);
                    }
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

        sair.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FirebaseAuth.getInstance().signOut();
                Intent intent = new Intent(MainActivity.this, SplashActivity.class);
                startActivity(intent);
            }
        });
    }
}