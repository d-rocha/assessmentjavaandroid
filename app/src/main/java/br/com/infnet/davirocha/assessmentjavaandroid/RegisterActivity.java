package br.com.infnet.davirocha.assessmentjavaandroid;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.firebase.FirebaseApp;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.Objects;
import java.util.UUID;

import br.com.infnet.davirocha.assessmentjavaandroid.Model.User;

public class RegisterActivity extends AppCompatActivity {

    private EditText cpf, celular;
    private Button salvar;

    private FirebaseAuth mAuth;
    private DatabaseReference databaseReference;
    private FirebaseDatabase firebaseDatabase;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        cpf = (EditText) findViewById(R.id.edt_CPF);
        celular = (EditText) findViewById(R.id.edt_Celular);
        salvar = (Button) findViewById(R.id.btn_salvar);

        FirebaseApp.initializeApp(RegisterActivity.this);
        firebaseDatabase = FirebaseDatabase.getInstance();
        databaseReference = firebaseDatabase.getReference();

        salvar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String id = Objects.requireNonNull(mAuth.getCurrentUser()).getUid();
                Intent i = getIntent();
                String email = i.getStringExtra("email");
                String nome = i.getStringExtra("nome");
                User user = new User();
                user.setId(mAuth.getUid());
                user.setCpf(cpf.getText().toString());
                user.setCelular(celular.getText().toString());
                user.setEmail(email);
                user.setNome(nome);

                databaseReference.child("user").child(user.getId()).setValue(user);
                Intent intent = new Intent(RegisterActivity.this, MainActivity.class);
                intent.putExtra("email",user.getEmail());
                startActivity(intent);
                finish();
            }
        });
    }
}