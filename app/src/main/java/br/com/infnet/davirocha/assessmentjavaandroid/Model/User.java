package br.com.infnet.davirocha.assessmentjavaandroid.Model;

public class User {

    private String id;
    private String nome;
    private String email;
    private String celular;
    private String cpf;

    public User() {
    }

    public User(String id, String nome, String email, String celular, String cpf) {
        this.id = id;
        this.nome = nome;
        this.email = email;
        this.celular = celular;
        this.cpf = cpf;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getCelular() {
        return celular;
    }

    public void setCelular(String celular) {
        this.celular = celular;
    }

    public String getCpf() {
        return cpf;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }
}
